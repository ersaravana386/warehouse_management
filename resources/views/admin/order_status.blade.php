@extends('admin.layouts.app')
<style>
.br-section-wrapper-form {
    background-color: #e9ecef;
    padding: 20px 20px 10px 20px;
    border-radius: 5px;
}
</style>
@section('content')
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="{{route('dashboard')}}">Home</a>
          <span class="breadcrumb-item active">Order  Management</span>
        </nav>
      </div><!-- br-pageheader -->
     




      <div class="br-pagebody">
        <div class="br-section-wrapper bd">
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">Id</th>
                  <th class="wd-15p">Product Name</th>
                  <th class="wd-15p">Product Image</th>
                  <th class="wd-15p">Price</th>
                  <th class="wd-15p">Company Name</th>
                  <th class="wd-20p">Quantity</th> 
                  <th class="wd-15p">Customer</th>
                  <th class="wd-20p">Status</th> 
                  <th class="wd-15p">Comments</th>
                  
                </tr>
              </thead>
              <tbody>
              @foreach ($orders as $key=> $row)
                <tr>
                  <td>{{$loop->iteration}}</td>
                  <td>{{$row->product_name}}</td>


                  <td> 
                  <a target="_blank" href="{{asset($row->image)}}">
                  <img src="{{asset($row->image)}}" style="width:60px; height:60px;">
                  </a>
                  </td>


                  <td>{{$row->price}}</td>
                  <td>{{$row->company_name}}</td>
                  <td>{{$row->quantity}}</td>
                  <td>{{$row->name}}<br><small>{{$row->mobile}}</small></td>
                   <td>@if($row->status == 1)<b style="color:green;">Completed</b>@else<b style="color:red;">Not Completed</b>@endif</td>
                   <td>{{$row->comment}}</td>
                  </td>
                </tr>

            @endforeach
            </table>
          </div><!-- table-wrapper -->
        </div><!-- br-section-wrapper -->
      </div><!-- br-pagebody -->


      <footer class="br-footer">
        <div class="footer-left">
         
        </div>
        <div class="footer-right d-flex align-items-center">
           {{--<span class="tx-uppercase mg-r-10">Share:</span>
          <a target="_blank" class="pd-x-5" href="https://www.facebook.com/sharer/sharer.php?u=http%3A//themepixels.me/bracketplus/intro"><i class="fab fa-facebook tx-20"></i></a>
          <a target="_blank" class="pd-x-5" href="https://twitter.com/home?status=Bracket%20Plus,%20your%20best%20choice%20for%20premium%20quality%20admin%20template%20from%20Bootstrap.%20Get%20it%20now%20at%20http%3A//themepixels.me/bracketplus/intro"><i class="fab fa-twitter tx-20"></i></a> --}}
        </div>
      </footer>

        <!-- Edit Modal -->
  
  
@endsection
@section('scripts')
    <script>
        $('#management').addClass('active')
        $('#order_status').addClass('active')
        $('#site_title').html(' | Order Management ')
    </script>
    <script>
        $('#file').on('change',function(){
          //get the file name
          var fileName = $(this).val();
          //replace the "Choose a file" label
          $(this).next('#file_label').html(fileName);
        });
    </script>
    <script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script>
       

        $(function(){
          'use strict';

          $('#datatable1').DataTable({
            stateSave: true,
            responsive: true,
            language: {
              searchPlaceholder: 'Search...',
              sSearch: '',
              lengthMenu: '_MENU_ items/page',
            },
            "paging": true,
            "ordering": true,
            "searching": true,
            "info": false,
            "bLengthChange" : false, 
          });

          // Select2
          $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });

        function editWarehouse(id, name , email , location, pincode) {
          $('#editForm').trigger('reset')
              $("input[name='name']").val(name)
              $("input[name='email']").val(email)
              $("input[name='location']").val(location)
              $("input[name='pincode']").val(pincode)
              $("input[name='id']").val(id)
              $("#editModal").modal({
                backdrop: 'static',
                keyboard: false
              });
        }

        $('#editForm').validate({
            normalizer: function(value) {
                    return $.trim(value);
            },
            ignore: [],
            rules : {
                name : {
                    required: true,
                },
                warehouse_address : {
                    required: true,
                },
               
            },
            messages : {
                name : {
                    required: "Please Enter Name",
                },
                warehouse_address : {
                    required: "Please enter WareHouse Address",
                },
               
            },
            submitHandler : function(form){
                var form = document.getElementById('editForm');
                var formData = new FormData(form);
                $('.addBtn').prop('disabled', true)
                $.ajax({
                    data : formData,
                    type: "post",
                    url : "{{route('update_warehouse')}}",
                    processData : false,
                    dataType: "json",
                    contentType: false,
                    cache: false, 
                    processData: false,
                    async: true,
                    headers: {
                        'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: function() {
                        $(".addBtn").text("Updating..");  
                    },
                    success : function(data){
                        $('.addBtn').text('Update')
                        $('.addBtn').prop('disabled', false)
                        if(data.status == 1){
                          toastr["success"](data.response);
                          setTimeout(function() {      
                                window.location.href=""
                            }, 1000);
                        }
                        else if(data.status == 10){
                          toastr["success"](data.response);
                         
                        }
                        else{
                            var html =""
                            $.each(data.response,function(key,value){
                                html += value + '</br>';
                            });
                            toastr["error"](html);
                        }
                    }
                });
            }
        })
      
        
        $(".delete-button").click(function(){
            $("#deleteModal").modal();
            $('.delete-class').data('delete-url', $(this).data('url'));
        });
        $(".delete-class").click(function(){
            var url = $(this).data('delete-url');
            $('.deleteBtn').prop('disabled', true)
            $.ajax({
                type : "GET",
                url : url,
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend: function() {
                  $(".deleteBtn").text("Changing..");  
                },
                success: function(data) {
                    $(".deleteBtn").text("Changed"); 
                    $('.deleteBtn').prop('disabled', false)
                    if (data.status == true) {
                        toastr["success"](data.response);
                        setTimeout(function() {      
                                window.location.href=""
                        },1000);
                    } else {
                        if (data.response) {
                        toastr["error"](data.response);
                        }
                    }
                }
            })
        })

        $(function(){
          'use strict'

          // FOR DEMO ONLY
          // menu collapsed by default during first page load or refresh with screen
          // having a size between 992px and 1299px. This is intended on this page only
          // for better viewing of widgets demo.
          $(window).resize(function(){
            minimizeMenu();
          });

          minimizeMenu();

          function minimizeMenu() {
            if(window.matchMedia('(min-width: 992px)').matches && window.matchMedia('(max-width: 1299px)').matches) {
              // show only the icons and hide left menu label by default
              $('.menu-item-label,.menu-item-arrow').addClass('op-lg-0-force d-lg-none');
              $('body').addClass('collapsed-menu');
              $('.show-sub + .br-menu-sub').slideUp();
            } else if(window.matchMedia('(min-width: 1300px)').matches && !$('body').hasClass('collapsed-menu')) {
              $('.menu-item-label,.menu-item-arrow').removeClass('op-lg-0-force d-lg-none');
              $('body').removeClass('collapsed-menu');
              $('.show-sub + .br-menu-sub').slideDown();
            }
          }
        });
    </script>


@endsection