<!DOCTYPE html>
<html lang="en">
    <head>
    <title>Admin Panel</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        {{-- <!-- Twitter -->
        <meta name="twitter:site" content="@themepixels">
        <meta name="twitter:creator" content="@themepixels">
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:title" content="Bracket Plus">
        <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
        <meta name="twitter:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
        <!-- Facebook -->
        <meta property="og:url" content="http://themepixels.me/bracketplus">
        <meta property="og:title" content="Bracket Plus">
        <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">
        <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
        <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
        <meta property="og:image:type" content="image/png">
        <meta property="og:image:width" content="1200">
        <meta property="og:image:height" content="600">
        <!-- Meta -->
        <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
        <meta name="author" content="ThemePixels"> --}}
  
        <!-- vendor css -->
        <link href="{{asset('assets/lib/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
        <!-- Bracket CSS -->
        <!-- <link rel="stylesheet" href="{{asset('assets/css/gold.css')}}"> -->
        <link rel="stylesheet" href="{{asset('assets/css/bracket.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/bracket.oreo.css')}}">
    </head>
    <body>










        <div class="d-flex align-items-center justify-content-center ht-100v">
        
            <form role="form" method="post" action="javascript:;" id="loginForm">
                <div class="overlay-body d-flex align-items-center justify-content-center">
                    <div class="login-wrapper wd-300 wd-xs-350 pd-25 pd-xs-40 bd bd-white-2">
                  
                    <div class="tx-white-5 tx-center mg-b-25">Create Account</div>
                    <div class="msgDiv"></div>

                    <div class="form-group">
                    <label for="">Warehouse Name</label>
                        <input type="text" name="name" id="name" class="form-control fc-outline-dark" placeholder="Enter your Warehouse Name"  autocomplete="off" >
                    </div><!-- form-group -->

                    <div class="form-group">
                    <label for="">Location</label>
                        <input type="text" name="location" id="location" class="form-control fc-outline-dark" placeholder="Enter Location"  autocomplete="off" >
                    </div><!-- form-group -->


                    <div class="form-group">
                    <label for="">Pincode</label>
                        <input type="text" name="pincode" id="pincode" class="form-control fc-outline-dark" placeholder="Enter Pincode"  autocomplete="off" >
                    </div><!-- form-group -->


                    <div class="form-group">
                   
                    <div class="form-group">
                    <label for="">Email</label>
                        <input type="email" name="email" id="email" class="form-control fc-outline-dark" placeholder="Enter your email" autocomplete="off" id="cemail"  onblur="checkmain(this.value)">
                                </div><!-- form-group -->

                                <div class="msgDiv3"></div>

<br>

                    <div class="form-group">
                    <label for="">Password</label>
                        <input type="password" name="password" id="password" class="form-control fc-outline-dark" placeholder="Enter your Password" autocomplete="off">
                      
                                </div><!-- form-group -->

                                <p id="passwordHelpBlock" class="form-text text-muted">
                                <small>
                                Your password must be more than 8 characters long, 
                                    should contain at-least 1 Uppercase,
                                    1 Numeric and 1 special character.
                                </small>
                                    
                                </p>


                    
                    <div class="form-group" >
                    <label for="">Confirm Password</label>
                        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control fc-outline-dark" placeholder="Enter your Confirm password" autocomplete="off">
                       <div class="row">
                       <div class="col-md-12">
                       <a href="{{route('login')}}" class="tx-primary tx-12 d-block mg-t-10" >Already Registered ? Sign In</a>
                       </div>
                      
                       </div>
                       
                        
                    </div><!-- form-group -->

                    <div class="msgDiv1"></div>

                    

                
                    </div><!-- login-wrapper -->
                </div><!-- overlay-body -->
            </form>
        </div><!-- d-flex -->


        <script src="{{asset('assets/lib/jquery/jquery.min.js')}}"></script>
        <script src="{{asset('assets/lib/jquery-ui/ui/widgets/datepicker.js')}}"></script>
        <script src="{{asset('assets/lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        <!-- <script src="{{asset('js/jquery.validate.js')}}"></script> -->
        <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
        <script>
            $("#loginForm").validate({
                normalizer: function (value) {
                    return $.trim(value);
                },
                rules: {
                    name: {
                        required: true,
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
                    },
                    password_confirmation: {
                        required: true
                    }

                },
                messages: {
                    name: {
                        required: 'Name is  required',
                    },
                    email: {
                        required: 'Email is  required',
                        email: 'Email is invalid'
                    },
                    password: {
                        required: 'Password is required'
                    },
                    password_confirmation: {
                        required: 'Confirm Password is required'
                    }
                },
                submitHandler: function (form) {
                    var form = document.getElementById('loginForm');
                    var data = new FormData(form);
                    $('.msgDiv').html(``);
                    $.ajax({
                        type: "POST",
                        url: "{{route('insert_account')}}",
                        data: $(form).serialize(),
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (data) {
                            if (data.status == 1) {

                                $('.msgDiv').html(` <div class="alert alert-danger">` + data.response + `</div> `);
                                setTimeout(function () {
                                    location.href = "{{route('login')}}";
                                }, 3000);
                                
                            } else {
                                $('.msgDiv').html(` <div class="alert alert-danger">` + data.response + `</div> `);
                                setTimeout(function () {
                                    location.href = "{{route('create_account')}}";
                                }, 3000);
                            }
                        }
                    });
                    return false;
                }
            });
        </script>    

<script>
    function checkmain(email)
{
  
$.ajax({
  url:"{{ route('availability_mail') }}",
type: 'POST',
data: { email: email ,  "_token": "{{ csrf_token() }}" },
}).done(function(data) {
  if(data.status == 1)
  {
  $('.msgDiv3').html(`<div style="color:green;">` + data.response + `</div>`);
  $('.msgDiv1').html(`<button disabled type="submit"  id=""  class="btn btn-white btn-block">Add Admin</button>`);

  
  }
  else
    {
      $('.msgDiv3').html(`<div  style="color:green;">` + data.response + `</div>`);
      $('.msgDiv1').html(`<button type="submit"  id=""  class="btn btn-white btn-block" >Add Admin</button>`);
    }


});
}
</script>




    </body>
</html>