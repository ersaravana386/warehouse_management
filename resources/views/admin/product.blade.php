@extends('admin.layouts.app')
<style>
.br-section-wrapper-form {
    background-color: #e9ecef;
    padding: 30px 20px;
  
    border-radius: 3px;
}
</style>
@section('content')

<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css"; rel="stylesheet" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js";></script> -->

      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="{{route('dashboard')}}">Home</a>
          <span class="breadcrumb-item active">Product Management</span>
        </nav>
      </div><!-- br-pageheader -->
     
                            <script>
                           function isNumberKey(evt){ 
                                var charCode = (evt.which) ? evt.which : event.keyCode 
                                if (charCode > 31 && (charCode < 48 || charCode > 57)) 
                                    return false; 
                                return true; 
                            } 
                            </script>  

<br>





      <div class="br-pagebody">
        <div class="br-section-wrapper-form">
          

          <div class="table-wrapperbd">
          <form action="javascript:;" id="product-form" method="POST">
                   <div class="row">

                        <div class="col-lg-3">
                            <div class="custom-file">
                              <label>Product Name</label>
                                <input type="text" name="pname"  class="form-control" placeholder="Product Name" autocomplete="off" >
                                </div>
                             
                        </div>


                        <div class="col-lg-3">
                            <div class="custom-file">
                            <label>Price</label>
                                <input type="text" name="price" class="form-control" placeholder="Price" autocomplete="off"  onkeypress="return isNumberKey(event)"  >
                            </div>
                            <div class="msgDiv"></div>
                        </div>

                        <div class="col-lg-3">
                            <div class="custom-file">
                            <label>Offer</label>
                                <input type="text" name="offer" class="form-control" placeholder="Offer" autocomplete="off"  value="0"  readonly> 
                            </div>
                        </div>
                      
                        <div class="col-lg-3">
                            <div class="custom-file">
                              <label>Product Image</label>
                                <input type="file" name="image" id="image"   class="form-control"  required  >
                                </div>
                             
                        </div>

                      
                      
                      
                    </div>    
<br><br><br>
                    <div class="row">


                    <div class="col-lg-3">
                            <div class="custom-file">
                            <label>Stock</label>
                            <input type="text" name="stock" id="stock" class="form-control" placeholder="Stock" autocomplete="off">
                            </div>
                        </div>


                    <div class="col-lg-3">
                            <div class="custom-file">
                            <label>Company Name</label>
                            <input type="text" name="company_name" id="company_name" class="form-control" placeholder="Company Name" autocomplete="off">
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="custom-file">
                            <label>Expire Date</label>
                            <input type="date" name="expire_date" id="salary" class="form-control" placeholder="Expire Date" onkeypress="return isNumberKey(event)" autocomplete="off"  >
                            </div>
                        </div>

                        <div class="col-lg-3">
                        <div class="custom-file">
                              <label for ="Type" style=" color:#8b008b;"><b> Select Type</b></label>
                            <select id="js-example-basic-single" name="type" class="form-control" style="height: 42px;">
                            <option value="" selected>Select Type</option>
                            <option value="1" >Cookies</option>
                            <option value="2" >Fruits</option>
                            <option value="3" >Oils</option>
                          
                            </select>
                            </div>
                        </div>

                   
                        
                      
                        <div class="col-lg-3" style="padding:28px;">
                     
                        <button type="submit"  id="" class="btn btn-primary addBtn">Add Product</button>
                               
                      
                    </div>     </div>


            </form>  

          </div><!-- table-wrapper -->
        </div><!-- br-section-wrapper -->
      </div><!-- br-pagebody -->



@if ($errors->any())
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
        </div>
        @endif
<br>




      

  



      <div class="br-pagebody">
        <div class="br-section-wrapper bd">
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
            <thead>
                <tr>
                <th class="">ID</th>
                  <th class="">Product Name</th>
                  <th class="">Price</th>
                  <th class="">Stock</th>
                  <th class="">Offer</th>
                  <th class="">Company Name</th>
                  <th class="">Expire Date</th>
                  <th class="">Type</th>
                  <th class="">Image</th>
                  <th class="">Action</th>
                </tr>
              </thead>
              <tbody>
              @foreach($product as $row)
                <tr>  
                <td>{{$loop->iteration}}</td>
                  <td>{{$row->product_name}}</td>
                  <td>{{$row->price}}</td>
                  <td>{{$row->stock}}</td>
                  <td>{{$row->offer}}</td>
                  <td>{{$row->company_name}}</td>
                  <td>{{$row->expire_date}}</td>
                  <td>@if($row->type == 1)Cookies @elseif($row->type == 2)Fruits @else Oils @endif </td>
                  <td>   <a target="_blank" href="{{asset($row->image)}}"><img src="{{asset($row->image)}}" style="width:60px;"></a></td>
                  <td>
                       <button class="btn btn-outline-primary btn-xs edit-button" onclick="editProduct('{{$row->id}}', '{{$row->product_name}}', '{{$row->price}}', '{{$row->offer}}', '{{$row->company_name}}', '{{$row->expire_date}}', '{{$row->stock}}')" title="Edit">
                       <i class="fa fa-pencil" title="Edit" data-toggle="tooltip"></i>
                       </button>

                       <button class="btn btn-outline-danger btn-xs delete-button" data-url="{{route('deleteProduct',['id'=>$row->id])}}">
                           <i class="fa fa-trash-o" title="Delete" data-toggle="tooltip"></i>
                       </button>
                    </td>
                 
                </tr>

            @endforeach
            </table>
          </div><!-- table-wrapper -->
        </div><!-- br-section-wrapper -->
      </div><!-- br-pagebody -->







      <footer class="br-footer">
        <div class="footer-left">
         
        </div>
        <div class="footer-right d-flex align-items-center">
           {{--<span class="tx-uppercase mg-r-10">Share:</span>
          <a target="_blank" class="pd-x-5" href="https://www.facebook.com/sharer/sharer.php?u=http%3A//themepixels.me/bracketplus/intro"><i class="fab fa-facebook tx-20"></i></a>
          <a target="_blank" class="pd-x-5" href="https://twitter.com/home?status=Bracket%20Plus,%20your%20best%20choice%20for%20premium%20quality%20admin%20template%20from%20Bootstrap.%20Get%20it%20now%20at%20http%3A//themepixels.me/bracketplus/intro"><i class="fab fa-twitter tx-20"></i></a> --}}
        </div>
      </footer>

        <!-- Edit Modal -->
      <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title text-center" id="myModalLabel">Edit  Details</h4>
            </div>
            <form class="form-client" id="editForm" action="javascript:;">
              <div class="modal-body">
                <div class="form-group">
                    <label>Product Name</label>
                    <input type="text" class="form-control" name="id" id="id" value="" style="display:none;" >
                    <input class="form-control" id="edit_product_name" name="product_name" type="text" autocomplete="off" >
                  </div>

                  <div class="form-group">
                    <label>Price</label>
                  
                    <input class="form-control" id="edit_price" name="price" type="text" autocomplete="off" >
                  </div>

                  <div class="form-group">
                    <label>Stock</label>
                  
                    <input class="form-control" id="edit_stock" name="stock" type="text" autocomplete="off" >
                  </div>

                  <div class="form-group">
                    <label>Offer </label>
                    <input class="form-control" id="edit_offer" name="offer" type="text"  autocomplete="off" readonly >
                  </div>
                  <div class="form-group">
                    <label>Company Name</label>
                    <input class="form-control" id="edit_company_name" name="company_name" type="text"  autocomplete="off" >
                  </div>
                  <div class="form-group">
                    <label>Expire Date</label>
                  <input class="form-control" id="edit_expire_date" name="expire_date" type="date" autocomplete="off" >
                 
                  </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="window.location.reload();">Close</button>
                <button type="submit" class="btn btn-primary button-save updateBtn">Update</button>
              </div>
           </form>
          </div>
        </div>
      </div>

        <!-- Delete Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content delete-popup">
          <div class="modal-header">
            <h4 class="modal-title">Delete Product Record</h4>
          </div>
          <div class="modal-body">
            Are you sure you want to delete?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" onclick="window.location.reload();">Close</button>
            <button type="button" class="btn btn-primary delete-class deleteBtn" data-delete-url="">Delete</button>
          </div>
        </div>
      </div>
    </div>
    
@endsection
@section('scripts')
    <script>
         $('#management').addClass('active')
        $('#product').addClass('active')
        $('#site_title').html(' | Product Management ')
    </script>
    <script>
        $('#file').on('change',function(){
          //get the file name
          var fileName = $(this).val();
          //replace the "Choose a file" label
          $(this).next('#file_label').html(fileName);
        });
    </script>
    <script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script>
        $('#product-form').validate({
            normalizer: function(value) {
                    return $.trim(value);
            },
            ignore: [],
            
            submitHandler : function(form){
                var form = document.getElementById('product-form');
                var formData = new FormData(form);
                $('.addBtn').prop('disabled', true)
                $.ajax({
                    data : formData,
                    type: "post",
                    url : "{{route('insert_product')}}",
                    processData : false,
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    processData: false,
                    async: true,
                    headers: {
                        'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: function() {
                        $(".addBtn").text("Saving..");  
                    },
                    success : function(data){
                        $('.addBtn').text('Add Product')
                        $('.addBtn').prop('disabled', false)
                        if(data.status == 1){
                          toastr["success"](data.response);
                          setTimeout(function() {      
                            location.href=""
                            }, 1000);
                        }else{
                            var html =""
                            $.each(data.response,function(key,value){
                                html += value + '</br>';
                            });
                            toastr["error"](html);
                        }
                    }
                });
            }
        })

        $(function(){
          'use strict';

          $('#datatable1').DataTable({
            stateSave: true,
            responsive: true,
            language: {
              searchPlaceholder: 'Search...',
              sSearch: '',
              lengthMenu: '_MENU_ items/page',
              
            },
            "paging": true,
            "ordering": true,
            "searching": true,
            "info": false,
            "bLengthChange" : false, 
          });

          // Select2
          $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });

        function editProduct(id, product_name, price , offer , company_name ,expire_date , stock) {
              $('#editForm').trigger('reset')
              $("input[name='product_name']").val(product_name)
             
              $("input[name='id']").val(id)
              $("input[name='price']").val(price)
              $("input[name='offer']").val(offer)
              $("input[name='stock']").val(stock)
              $("input[name='company_name']").val(company_name)
              $("input[name='expire_date']").val(expire_date)
        
              $("#editModal").modal({
                backdrop: 'static',
                keyboard: false
              });
        }

        $('#editForm').validate({
            normalizer: function(value) {
                    return $.trim(value);
            },
            ignore: [],
          
            submitHandler : function(form){
                var form = document.getElementById('editForm');
                var formData = new FormData(form);
                $('.addBtn').prop('disabled', true)
                $.ajax({
                    data : formData,
                    type: "post",
                    url : "{{route('update_product')}}",
                    processData : false,
                    dataType: "json",
                    contentType: false,
                    cache: false, 
                    processData: false,
                    async: true,
                    headers: {
                        'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: function() {
                        $(".addBtn").text("Updating..");  
                    },
                    success : function(data){
                        $('.addBtn').text('Update')
                        $('.addBtn').prop('disabled', false)
                        if(data.status == 1){
                          toastr["success"](data.response);
                          setTimeout(function() {      
                                window.location.href=""
                            }, 1000);
                        }else{
                            var html =""
                            $.each(data.response,function(key,value){
                                html += value + '</br>';
                            });
                            toastr["error"](html);
                        }
                    }
                });
            }
        })
      
        
        $(".delete-button").click(function(){
            $("#deleteModal").modal();
            $('.delete-class').data('delete-url', $(this).data('url'));
        });
        $(".delete-class").click(function(){
            var url = $(this).data('delete-url');
            $('.deleteBtn').prop('disabled', true)
            $.ajax({
                type : "GET",
                url : url,
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend: function() {
                  $(".deleteBtn").text("Deleting..");  
                },
                success: function(data) {
                    $(".deleteBtn").text("Delete"); 
                    $('.deleteBtn').prop('disabled', false)
                    if (data.status == true) {
                        toastr["success"](data.response);
                        setTimeout(function() {      
                                window.location.href=""
                        },1000);
                    } else {
                        if (data.response) {
                        toastr["error"](data.response);
                        }
                    }
                }
            })
        })

        $(function(){
          'use strict'

          // FOR DEMO ONLY
          // menu collapsed by default during first page load or refresh with screen
          // having a size between 992px and 1299px. This is intended on this page only
          // for better viewing of widgets demo.
          $(window).resize(function(){
            minimizeMenu();
          });

          minimizeMenu();

          function minimizeMenu() {
            if(window.matchMedia('(min-width: 992px)').matches && window.matchMedia('(max-width: 1299px)').matches) {
              // show only the icons and hide left menu label by default
              $('.menu-item-label,.menu-item-arrow').addClass('op-lg-0-force d-lg-none');
              $('body').addClass('collapsed-menu');
              $('.show-sub + .br-menu-sub').slideUp();
            } else if(window.matchMedia('(min-width: 1300px)').matches && !$('body').hasClass('collapsed-menu')) {
              $('.menu-item-label,.menu-item-arrow').removeClass('op-lg-0-force d-lg-none');
              $('body').removeClass('collapsed-menu');
              $('.show-sub + .br-menu-sub').slideDown();
            }
          }
        });
    </script>
    <script>$(document).ready(function() {
    $('#js-example-basic-single').select2();
});</script>




@endsection