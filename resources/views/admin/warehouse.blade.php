@extends('admin.layouts.app')
<style>
.br-section-wrapper-form {
    background-color: #e9ecef;
    padding: 20px 20px 10px 20px;
    border-radius: 5px;
}
</style>
@section('content')
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="{{route('dashboard')}}">Home</a>
          <span class="breadcrumb-item active">Warehouse Management</span>
        </nav>
      </div><!-- br-pageheader -->
     




      <div class="br-pagebody">
        <div class="br-section-wrapper bd">
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">Id</th>
                  <th class="wd-15p">Warehouse Name</th>
                  <th class="wd-15p">Email</th>
                  <th class="wd-15p">Location</th>
                  <th class="wd-20p">Pincode</th> 
                  <th class="wd-20p">Status</th> 
                  <th class="wd-15p">Action</th>
                </tr>
              </thead>
              <tbody>
          @foreach($warehouse as $row)
                <tr>
                  <td>{{$loop->iteration}}</td>
                  <td>{{$row->name}}</td>
                  <td>{{$row->email}}</td>
                  <td>{{$row->location}}</td>
                  <td>{{$row->pincode}}</td>
                   <td>@if($row->active == 1)<b style="color:green;">Active</b>@else<b style="color:red;">Inactive</b>@endif</td>
                  <td>
                       <button class="btn btn-outline-primary btn-xs edit-button" onclick="editWarehouse('{{$row->id}}', '{{$row->name}}', '{{$row->email}}', '{{$row->location}}', '{{$row->pincode}}')" title="Edit">
                       <i class="fa fa-pencil" title="Edit" data-toggle="tooltip"></i>
                       </button>
                       @if($row->active == 1)

                       <button class="btn btn-outline-danger btn-xs delete-button" data-url="{{route('deleteWarehouse',['id'=>$row->id])}}" title="Inactivate">
                           <i class="fa fa-eye-slash" title="Inactivate" data-toggle="tooltip"></i>
                       </button>
                       @else
                       <button class="btn btn-outline-danger btn-xs delete-button" data-url="{{route('deleteWarehouse2',['id'=>$row->id])}}" title="Activate">
                           <i class="fa fa-eye" title="Activate" data-toggle="tooltip"></i>
                       </button>
                       @endif

                       
                    </td>
                  </td>
                </tr>

            @endforeach
            </table>
          </div><!-- table-wrapper -->
        </div><!-- br-section-wrapper -->
      </div><!-- br-pagebody -->


      <footer class="br-footer">
        <div class="footer-left">
         
        </div>
        <div class="footer-right d-flex align-items-center">
           {{--<span class="tx-uppercase mg-r-10">Share:</span>
          <a target="_blank" class="pd-x-5" href="https://www.facebook.com/sharer/sharer.php?u=http%3A//themepixels.me/bracketplus/intro"><i class="fab fa-facebook tx-20"></i></a>
          <a target="_blank" class="pd-x-5" href="https://twitter.com/home?status=Bracket%20Plus,%20your%20best%20choice%20for%20premium%20quality%20admin%20template%20from%20Bootstrap.%20Get%20it%20now%20at%20http%3A//themepixels.me/bracketplus/intro"><i class="fab fa-twitter tx-20"></i></a> --}}
        </div>
      </footer>

        <!-- Edit Modal -->
      <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title text-center" id="myModalLabel">Edit WareHouse Details</h4>
            </div>
            <form class="form-client" id="editForm" action="javascript:;">
            <div class="modal-body">
              

              <div class="form-group">
                <label>Warehouse</label>
                <input type="text" class="form-control" name="id" id="id" value="" style="display:none;" >
                <input class="form-control" id="edit_name" name="name" type="text" autocomplete="off" >
              </div>
              <div class="form-group">
                <label>Email</label>
                <input class="form-control" id="edit_email" name="email" type="text" autocomplete="off" >
              </div>
              <div class="form-group">
                <label>Location</label>
                <input class="form-control" id="edit_location" name="location" type="text" autocomplete="off" >
              </div>

              <div class="form-group">
                <label>Pincode</label>
                <input class="form-control" id="edit_pincode" name="pincode" type="text" autocomplete="off" >
              </div>

            
          </div>
          
              <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="window.location.reload();">Close</button>
                <button type="submit" class="btn btn-primary button-save updateBtn">Update</button>
              </div>
           </form>
          </div>
        </div>
      </div>

        <!-- Delete Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content delete-popup">
          <div class="modal-header">
            <h4 class="modal-title">Change Status on  Warehouse</h4>
          </div>
          <div class="modal-body">
            Are you sure you want to Change Status?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" onclick="window.location.reload();">Close</button>
            <button type="button" class="btn btn-primary delete-class deleteBtn" data-delete-url="">Change</button>
          </div>
        </div>
      </div>
    </div>
@endsection
@section('scripts')
    <script>
        $('#management').addClass('active')
        $('#warehouse').addClass('active')
        $('#site_title').html(' | Warehouse Management ')
    </script>
    <script>
        $('#file').on('change',function(){
          //get the file name
          var fileName = $(this).val();
          //replace the "Choose a file" label
          $(this).next('#file_label').html(fileName);
        });
    </script>
    <script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script>
       

        $(function(){
          'use strict';

          $('#datatable1').DataTable({
            stateSave: true,
            responsive: true,
            language: {
              searchPlaceholder: 'Search...',
              sSearch: '',
              lengthMenu: '_MENU_ items/page',
            },
            "paging": true,
            "ordering": true,
            "searching": true,
            "info": false,
            "bLengthChange" : false, 
          });

          // Select2
          $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });

        function editWarehouse(id, name , email , location, pincode) {
          $('#editForm').trigger('reset')
              $("input[name='name']").val(name)
              $("input[name='email']").val(email)
              $("input[name='location']").val(location)
              $("input[name='pincode']").val(pincode)
              $("input[name='id']").val(id)
              $("#editModal").modal({
                backdrop: 'static',
                keyboard: false
              });
        }

        $('#editForm').validate({
            normalizer: function(value) {
                    return $.trim(value);
            },
            ignore: [],
            rules : {
                name : {
                    required: true,
                },
                warehouse_address : {
                    required: true,
                },
               
            },
            messages : {
                name : {
                    required: "Please Enter Name",
                },
                warehouse_address : {
                    required: "Please enter WareHouse Address",
                },
               
            },
            submitHandler : function(form){
                var form = document.getElementById('editForm');
                var formData = new FormData(form);
                $('.addBtn').prop('disabled', true)
                $.ajax({
                    data : formData,
                    type: "post",
                    url : "{{route('update_warehouse')}}",
                    processData : false,
                    dataType: "json",
                    contentType: false,
                    cache: false, 
                    processData: false,
                    async: true,
                    headers: {
                        'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: function() {
                        $(".addBtn").text("Updating..");  
                    },
                    success : function(data){
                        $('.addBtn').text('Update')
                        $('.addBtn').prop('disabled', false)
                        if(data.status == 1){
                          toastr["success"](data.response);
                          setTimeout(function() {      
                                window.location.href=""
                            }, 1000);
                        }
                        else if(data.status == 10){
                          toastr["success"](data.response);
                         
                        }
                        else{
                            var html =""
                            $.each(data.response,function(key,value){
                                html += value + '</br>';
                            });
                            toastr["error"](html);
                        }
                    }
                });
            }
        })
      
        
        $(".delete-button").click(function(){
            $("#deleteModal").modal();
            $('.delete-class').data('delete-url', $(this).data('url'));
        });
        $(".delete-class").click(function(){
            var url = $(this).data('delete-url');
            $('.deleteBtn').prop('disabled', true)
            $.ajax({
                type : "GET",
                url : url,
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend: function() {
                  $(".deleteBtn").text("Changing..");  
                },
                success: function(data) {
                    $(".deleteBtn").text("Changed"); 
                    $('.deleteBtn').prop('disabled', false)
                    if (data.status == true) {
                        toastr["success"](data.response);
                        setTimeout(function() {      
                                window.location.href=""
                        },1000);
                    } else {
                        if (data.response) {
                        toastr["error"](data.response);
                        }
                    }
                }
            })
        })

        $(function(){
          'use strict'

          // FOR DEMO ONLY
          // menu collapsed by default during first page load or refresh with screen
          // having a size between 992px and 1299px. This is intended on this page only
          // for better viewing of widgets demo.
          $(window).resize(function(){
            minimizeMenu();
          });

          minimizeMenu();

          function minimizeMenu() {
            if(window.matchMedia('(min-width: 992px)').matches && window.matchMedia('(max-width: 1299px)').matches) {
              // show only the icons and hide left menu label by default
              $('.menu-item-label,.menu-item-arrow').addClass('op-lg-0-force d-lg-none');
              $('body').addClass('collapsed-menu');
              $('.show-sub + .br-menu-sub').slideUp();
            } else if(window.matchMedia('(min-width: 1300px)').matches && !$('body').hasClass('collapsed-menu')) {
              $('.menu-item-label,.menu-item-arrow').removeClass('op-lg-0-force d-lg-none');
              $('body').removeClass('collapsed-menu');
              $('.show-sub + .br-menu-sub').slideDown();
            }
          }
        });
    </script>


@endsection