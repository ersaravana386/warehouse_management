<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\CommonController;

//Customer Details
Route::post('/customer_register',[CommonController::class,'customer_register']);
Route::post('/customer_login',[CommonController::class,'customer_login']);
Route::get('/dashboard',[CommonController::class,'dashboard']);
Route::post('/cart_insert',[CommonController::class,'cart_insert']);
Route::post('/cart_history',[CommonController::class,'cart_history']);
Route::post('/order_status_updation',[CommonController::class,'order_status_updation']);
//Delivery Team
Route::post('/delivery_login',[CommonController::class,'delivery_login']);
Route::post('/delivery_dashboard',[CommonController::class,'delivery_dashboard']);
Route::post('/view_cart_details',[CommonController::class,'view_cart_details']);

