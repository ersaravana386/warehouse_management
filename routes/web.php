<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\admin\LoginController;


Route::get('/',[LoginController::class,'showLogin']);
Route::get('/login',[LoginController::class,'showLogin'])->name('showlogin');
Route::post('/login',[LoginController::class,'login'])->name('login');
Route::get('/create_account',[LoginController::class,'create_account'])->name('create_account');
Route::post('/insert_account',[LoginController::class,'insert_account'])->name('insert_account');
Route::post('/availability_mail',[LoginController::class,'availability_mail'])->name('availability_mail');


Route::group(['middleware' => 'auth:admin'], function(){
    Route::get('/dashboard',[LoginController::class,'dashboard'])->name('dashboard');


    Route::get('/product',[LoginController::class,'product'])->name('product');
    Route::post('/insert_product',[LoginController::class,'insert_product'])->name('insert_product');
    Route::post('/update_product',[LoginController::class,'update_product'])->name('update_product');
    Route::get('/deleteProduct',[LoginController::class,'deleteProduct'])->name('deleteProduct');
    Route::post('/update_product',[LoginController::class,'update_product'])->name('update_product');

    //Warehouse Management
    Route::get('/warehouse',[LoginController::class,'warehouse'])->name('warehouse');
    Route::get('/deleteWarehouse',[LoginController::class,'deleteWarehouse'])->name('deleteWarehouse');
    Route::get('/deleteWarehouse2',[LoginController::class,'deleteWarehouse2'])->name('deleteWarehouse2');
    Route::post('/update_warehouse',[LoginController::class,'update_warehouse'])->name('update_warehouse');



//Delivery Management
    Route::get('/team',[LoginController::class,'team'])->name('team');
    Route::post('/insert_team',[LoginController::class,'insert_team'])->name('insert_team');
    Route::post('/update_team',[LoginController::class,'update_team'])->name('update_team');
    Route::get('/deleteTeam',[LoginController::class,'deleteTeam'])->name('deleteTeam');

    //Order Status

  Route::get('/order_status',[LoginController::class,'order_status'])->name('order_status');

  Route::get('/overall_order_status',[LoginController::class,'overall_order_status'])->name('overall_order_status');

    Route::get('/logout',[LoginController::class,'logout'])->name('logout');

});


