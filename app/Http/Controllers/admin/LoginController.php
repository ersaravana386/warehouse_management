<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


use Validator;
use DB;
use App\Models\Admin;
use App\Models\Product;
use App\Models\Deliveryteam;


class LoginController extends Controller
{


    protected $guard = 'admin';

    public function showLogin(Request $req){
        return view('admin.login');
    }
    public function create_account(Request $request)
    {
        $data  = [
            'failures' =>''
        ];
        return view('admin.account', $data);
    }
    public function insert_account(Request $request)
        {
            $rules = [
                'name' => 'required',
                'email' => 'required|email',
                'password' => 'required|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
                'password_confirmation' => 'required| min:8',
                'location' => 'required',
                'pincode' => 'required'
            ];
            $messages = [
                'name.required' => 'Warehouse  Name is required',
                'email.required' => 'Email is required',
                'password.required' => 'Password is required',
                'password_confirmation' => 'Confirm Password is required',
                'location.required' => 'Location   is required',
                'pincode.required' => 'Pincode   is required'
            ];
            $validation = Validator::make($request->all(),$rules,$messages);
            if($validation->fails()){
                return response()->json(['status' => false , 'response' => $validation->errors()]);
            }
            try{

                $admin = new Admin();
                $admin->name = $request->name;
                $admin->email = $request->email;    
                $admin->password = bcrypt($request->password);
                $admin->original_pass = $request->password;
                $admin->location = $request->location;
                $admin->pincode = $request->pincode;
                $admin->role = 2;
                $admin->save();
                return response()->json(['status'=>1,'response'=>"Warehouse Created Successfully"]);

                } catch (\Exception $e) {
                    throw $e;
                    return response()->json(['status'=>0,'response'=>"Not Inserted Data"]);
                }
        }
     
    public function availability_mail(Request $request)
      {
            $email = $request->email;
            $emailcheck = Admin::where('email',$email)->count();
            if($emailcheck >= 1)
            {
                $arr = array('response' => 'Email  ID is Already Taken!', 'status' => '1');
            }
            else
                {
                    $arr = array('response' => 'Email  ID is Accepted!', 'status' => '0');
                }
                return Response()->json($arr);
        }    

    public function dashboard(){

       // echo Auth::user()->email;exit;
        return view('admin.dashboard');
       
    }
    public function login(Request $request){
        $rules = [
            'email' => 'required|email',
            'password' => 'required',
        ];
        $messages = [
            'email.required' => 'Email is required',
            'password.required' => 'Password is required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status' => false , 'response' => $validation->errors()]);
        }
        $remember_me = $request->has('remember_me') ? true : false; 
        if(auth()->guard('admin')
        ->attempt(['email'=>$request->email,'password'=>$request->password], $remember_me)){
            $admin = auth()->guard('admin')->user();
            Session::put('name',$admin->name);
            Session::put('email',$admin->email);
            Session::put('id',$admin->id);
            return response()->json(['status'=>1,'response'=>"Login success"]);
        }else{
            return response()->json(['status'=>0,'response'=>['Invalid Username or Password']]);
        }
    }
   
   

    public function logout() {
        Auth::logout();
     //   Session::flush();
        return redirect()->route('login');
    }


// Warehouse

public function product(Request $request)
    {
        $warehouse_id = Auth::user()->id;
        $product  = Product::where('warehouse_id',$warehouse_id)->get();
        $data = [
            'product' => $product
        ];
        return view('admin.product',$data);
    }

    
public function insert_product(Request $request)
    {
        $rules = [
            'pname' => 'required',
            'price' => 'required|numeric',
            'stock' => 'required|numeric',
            'offer'   => 'required|numeric',
            'company_name' => 'required',
            'expire_date' => 'required',
            'type' => 'required',
            'image' => 'mimes:jpeg,jpg,png|max:20000',
        ];
        $messages = [
            'pname.required' => 'Product  Name is required',
            'price.required' => 'Price is required',
            'stock.required' => 'Stock is required',
            'offer.required' => 'Offer is required',
            'company_name.required' => 'Company Name is required',
            'expire_date.required' => 'Expire Date  is required',
            'type.required' => 'Type  is required',
            'image.mimes' => "Invalid file format only .jpeg,jpg,png files are allowed",
            'image.max' => "Maxinum allowed size is 2mb",
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status' => false , 'response' => $validation->errors()]);
        }
        try{

            $a = mt_rand(100000,999999); 
            $product = new Product();
            $product->product_name = $request->pname;
            $product->price = $request->price;    
            $product->offer =$request->offer;    
            $product->company_name = $request->company_name;
            $product->expire_date = $request->expire_date;
            $product->warehouse_id = Auth::user()->id;
            $product->stock = $request->stock;    
            $product->type = $request->type;

            if (request()->image) {
                $imageName =  $a.time() . '.' . request()->image->getClientOriginalExtension();
                request()->image->move(public_path('images/'), $imageName);
            }
            $product->image = 'images/' . $imageName;


            $product->save();
            $arr = array('response' => 'Product Added Successfully!', 'status' => 1);
            return Response()->json($arr);

            } catch (\Exception $e) {
                throw $e;
                $arr = array('response' => 'Not Insert data!', 'status' => 0);
                return Response()->json($arr);
            }
    } 
    
 public function deleteProduct(Request $request)
    {
        if($request->id){
            try{
                Product::where('id',$request->id)->delete();
                $arr = array('response' => 'Product Deleted  Successfully!', 'status' => 1);
            }catch(\Exception $e){
                $arr = array('response' => 'Not Delete Records!', 'status' => 0);
            }
        }else{
            $arr = array('response' => 'ID Not Passed!', 'status' => 0);
        }
        return Response()->json($arr);
    }
 
public function update_product(Request $request)
    {
        $update = Product::where('id',$request->id)->update([
            'product_name' =>$request->product_name ,'price' =>$request->price,'offer' =>$request->offer,
            'company_name' =>$request->company_name,'expire_date' =>$request->expire_date,
            'stock' =>$request->stock
        ]);
        if($update == TRUE)
            {
                $arr = array('response' => 'Product Updated Successfully!', 'status' => 1);
            }
            else
                {
                    $arr = array('response' => 'Product Not Updated!', 'status' => 0);
                }
                return Response()->json($arr);
    }

    
    
  //Manage Warehouse Details
  
  public function warehouse(Request $request)
    {
        $warehouse  = Admin::where('role','!=',1)->get();
        $data = [
            'warehouse' => $warehouse
        ];
        return view('admin.warehouse',$data);
    }
    public function deleteWarehouse(Request $request)
    {
        if($request->id){
            try{
                Admin::where('id',$request->id)->update(['active'=>0]);
                $arr = array('response' => 'WareHouse Inactive Successfully!', 'status' => 1);
            }catch(\Exception $e){
                $arr = array('response' => 'Not Inactive WareHouse Please Check Details!', 'status' => 0);
            }
        }else{
            $arr = array('response' => 'ID Not Passed!', 'status' => 0);
        }
        return Response()->json($arr);
    } 
    public function deleteWarehouse2(Request $request)
    {
        if($request->id){
            try{
                Admin::where('id',$request->id)->update(['active'=>1]);
                $arr = array('response' => 'WareHouse Active Successfully!', 'status' => 1);
            }catch(\Exception $e){
                $arr = array('response' => 'Not Active WareHouse Please Check Details!', 'status' => 0);
            }
        }else{
            $arr = array('response' => 'ID Not Passed!', 'status' => 0);
        }
        return Response()->json($arr);
    } 
    
    public function update_warehouse(Request $request)
    {
        try{
       $update = Admin::where('id',$request->id)
       ->update(['name'=>$request->name, 'location'=>$request->location
       , 'email'=>$request->email, 'pincode'=>$request->pincode]);
       if($update){ 
        $arr = array('response' => 'Warehouse Updated Successfully!', 'status' => 1);
            }
            else
                {
                    $arr = array('response' => 'Not Updated Please Check Details!', 'status' => 0);
                }
            return Response()->json($arr);
        } catch(\Illuminate\Database\QueryException $e){
            $errorCode = $e->errorInfo[1];
            if($errorCode == '1062'){
                $arr = array('response' => 'Name already exist!', 'status' => 1);
                return Response()->json($arr);
            }
        }    
    }



    public function team(Request $request)
    {
        $deliveryteam  = Deliveryteam::get();
        $data = [
            'deliveryteam' => $deliveryteam
        ];
        return view('admin.delivery_team',$data);
    }


    public function insert_team(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required',
            'password'   => 'required',
            'pincode' => 'required',
        ];
        $messages = [
            'name.required' => ' Name is required',
            'email.required' => 'Email is required',
            'password.required' => 'Password is required',
            'pincode.required' => 'Pincode  is required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status' => false , 'response' => $validation->errors()]);
        }
        try{

            $deliveryteam = new Deliveryteam();
            $deliveryteam->name = $request->name;
            $deliveryteam->email = $request->email;    
            $deliveryteam->password =$request->password;    
            $deliveryteam->pincode = $request->pincode;
            $deliveryteam->save();
            $arr = array('response' => 'Record Added Successfully!', 'status' => 1);
            return Response()->json($arr);

            } catch (\Exception $e) {
                throw $e;
                $arr = array('response' => 'Not Insert data!', 'status' => 0);
                return Response()->json($arr);
            }
    } 
    
 public function deleteTeam(Request $request)
    {
        if($request->id){
            try{
                Deliveryteam::where('id',$request->id)->delete();
                $arr = array('response' => 'Record Deleted  Successfully!', 'status' => 1);
            }catch(\Exception $e){
                $arr = array('response' => 'Not Delete Records!', 'status' => 0);
            }
        }else{
            $arr = array('response' => 'ID Not Passed!', 'status' => 0);
        }
        return Response()->json($arr);
    }
 
public function update_team(Request $request)
    {
        $update = Deliveryteam::where('id',$request->id)->update([
            'name' =>$request->name ,'email' =>$request->email,'password' =>$request->password,
            'pincode' =>$request->pincode
        ]);
        if($update == TRUE)
            {
                $arr = array('response' => 'Record Updated Successfully!', 'status' => 1);
            }
            else
                {
                    $arr = array('response' => 'Record Not Updated!', 'status' => 0);
                }
                return Response()->json($arr);
    }

   
    
    public function order_status(Request $request)
        {
            $w_id = Auth::user()->id;

            $orders = DB::table('products')
            ->join('carts', 'carts.product_id', '=', 'products.id')
            ->join('customers', 'customers.id', '=', 'carts.customer_id')
            ->where('products.warehouse_id', '=', $w_id)
            ->get();
            //echo "<pre>";print_r($order);exit;
          return view('admin.order_status', compact('orders'));
          
        }

    public function overall_order_status(Request $request)
        {
            $orders = DB::table('products')
            ->join('carts', 'carts.product_id', '=', 'products.id')
            ->join('customers', 'customers.id', '=', 'carts.customer_id')
            ->get();
          return view('admin.overall_order_status', compact('orders'));

        }    



}
