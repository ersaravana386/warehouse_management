<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Validator;
use DB;
use App\Models\Customer;
use App\Models\Product;
use App\Models\Cart;
use App\Models\Deliveryteam;


class CommonController extends Controller
{
    public function customer_register(Request $request)
        {
            $rules = [
                'name'=> 'required',
                'email'  => 'required',
                'pincode'  => 'required',
                'mobile'  => 'required',
                'password'  => 'required',
            ];
            
            $messages = [
                'name.required' => "Name is required",
                'email.required'   => "email is required",
                'pincode.required'   => "pincode is required",
                'mobile.required'   => "Mobile is required",
                'password.required'   => "Password is required"
            ];
            $validation = Validator::make($request->all(),$rules,$messages);
            if($validation->fails()){
                return response()->json(['status'=>1 , 'response'=>implode(',',$validation->errors()->all())]);
            }

            try{

                $customer = new Customer();
                $customer->name = $request->name;
                $customer->email = $request->email;    
                $customer->password = $request->password;
                $customer->pincode = $request->pincode;
                $customer->mobile = $request->mobile;
                $customer->save();
                return response()->json(['status'=>1,'response'=>"Customer Created Successfully"]);

                } catch (\Exception $e) {
                    throw $e;
                    return response()->json(['status'=>0,'response'=>"Not Inserted Data"]);
                }

        }

        public function customer_login(Request $request)
        {
            $rules = [
                
                'email'  => 'required',
                'password'  => 'required',
            ];
            
            $messages = [
                'email.required'   => "email is required",
                'password.required'   => "Password is required"
            ];
            $validation = Validator::make($request->all(),$rules,$messages);
            if($validation->fails()){
                return response()->json(['status'=>1 , 'response'=>implode(',',$validation->errors()->all())]);
            }

            try{

                $data_count  = Customer::where('email',$request->email)->where('password',$request->password)->count();
                if($data_count!=0)
                    {
                        $customer = Customer::where('email',$request->email)->where('password',$request->password)->first();
                        return response()->json(['status'=>1,'response'=>$customer]);
                    }
                    else
                        {
                            return response()->json(['status'=>0,'response'=>"Please Check Details"]);
                        }   
                

                } catch (\Exception $e) {
                    throw $e;
                    return response()->json(['status'=>0,'response'=>"Please Check Details"]);
                }

        }

        public function dashboard(Request $request)
        {
                $data_count  = Product::count();
                if($data_count!=0)
                    {
                        $customer = Product::with(['warehouse'])->get();
                        return response()->json(['status'=>1,'response'=>$customer]);
                    }
                    else
                        {
                            return response()->json(['status'=>0,'response'=>"No Data Found"]);
                        }   
        }

     public function cart_insert(Request $request)
        {
            $rules = [
                'customer_id'=> 'required',
                'product_id'  => 'required',
                'quantity' =>'required',
            ];
            
            $messages = [
                'customer_id.required' => "Customer ID is required",
                'product_id.required'   => "Product ID is required",
                'quantity.required'   => "Quantity  is required",
            ];
            $validation = Validator::make($request->all(),$rules,$messages);
            if($validation->fails()){
                return response()->json(['status'=>1 , 'response'=>implode(',',$validation->errors()->all())]);
            }

            try{

                $a = Product::where('id',$request->product_id)->first();
                $stock = $a->stock;

                if($stock > $request->quantity)
                {
                    $count = Cart::where('customer_id',$request->customer_id)
                    ->where('product_id',$request->product_id)->where('status',0)->count();
                    if($count!=0)
                        {
                            return response()->json(['status'=>0,'response'=>"Already Added Product on this Cart"]);
                        }
                        else
                            {
                                $cart = new Cart();
                                $cart->customer_id = $request->customer_id;
                                $cart->product_id = $request->product_id;    
                                $cart->quantity = $request->quantity;    
                                $cart->save();
                                return response()->json(['status'=>1,'response'=>"Cart Added  Successfully"]);
                            } 
                        }
                        else
                            {
                                return response()->json(['status'=>0,'response'=>"Out os Stock"]);
                            } 
                        
                        


                

                } catch (\Exception $e) {
                    throw $e;
                    return response()->json(['status'=>0,'response'=>"Not Inserted Data"]);
                }
        } 
        
        public function cart_history(Request $request)
        {
                $data_count  = Cart::where('customer_id',$request->customer_id)->count();
                if($data_count!=0)
                    {
                        $cart =  Cart::query();
                        $cart = $cart->with('product');
                        $cart = $cart->where('customer_id',$request->customer_id);
                        $result= $cart->get();

                      
                        return response()->json(['status'=>1,'response'=>$result]);
                    }
                    else
                        {
                            return response()->json(['status'=>0,'response'=>"No Data Found"]);
                        }   
        } 
        
   public function delivery_login(Request $request)
    {
       
     
        $rules = [
                
            'email'  => 'required',
            'password'  => 'required',
        ];
        
        $messages = [
            'email.required'   => "email is required",
            'password.required'   => "Password is required"
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>1 , 'response'=>implode(',',$validation->errors()->all())]);
        }

        try{

            $data_count  = Deliveryteam::where('email',$request->user)->where('password',$request->password)->count();
            if($data_count!=0)
                {
                    $delivery = Deliveryteam::where('email',$request->user)->where('password',$request->password)->first();
                    return response()->json(['status'=>1,'response'=>$delivery]);
                }
                else
                    {
                        return response()->json(['status'=>0,'response'=>"Please Check Details"]);
                    }   
            

            } catch (\Exception $e) {
                throw $e;
                return response()->json(['status'=>0,'response'=>"Please Check Details"]);
            }


    } 
    
   public function delivery_dashboard(Request $request)
    {
        $id = $request->id;
        $get_pincode = Deliveryteam::where('id',$id)->first();
        $pincode = $get_pincode->pincode;

            $data_count  = Cart::count();
                if($data_count!=0)
                    {
                       
                        $cart =  Customer::query();
                        $cart = $cart->with('cart');
                        $cart = $cart->where('pincode',$pincode);
                        $result= $cart->get();
                        return response()->json(['status'=>1,'response'=>$result]);
                    }
                    else
                        {
                            return response()->json(['status'=>0,'response'=>"No Data Found"]);
                        }  

    }
    
  public function view_cart_details(Request $request)
    {

        $data_count  = Cart::where('customer_id',$request->customer_id)->count();
        if($data_count!=0)
            {
                $cart =  Cart::query();
                $cart = $cart->with('product');
                $cart = $cart->where('customer_id',$request->customer_id);
                $result= $cart->get();

              
                return response()->json(['status'=>1,'response'=>$result]);
            }
            else
                {
                    return response()->json(['status'=>0,'response'=>"No Data Found"]);
                }   
    }  

    public function order_status_updation(Request $request)
        {
            $rules = [
                'customer_id'=> 'required',
                'product_id'  => 'required',
                'comment' =>'required',
            ];
            
            $messages = [
                'customer_id.required' => "Customer ID is required",
                'product_id.required'   => "Product ID is required",
                'comment.required'   => "Comment ID is required",
            ];
            $validation = Validator::make($request->all(),$rules,$messages);
            if($validation->fails()){
                return response()->json(['status'=>1 , 'response'=>implode(',',$validation->errors()->all())]);
            }

            try{
                        $update = Cart::where('customer_id',$request->customer_id)
                        ->where('product_id',$request->product_id)
                        ->update(['comment'=>$request->comment, 'status'=>1]);

                       $product_stock1 = Product::where('id',$request->product_id)->first();
                       $product_stock = $product_stock1->stock;

                       $cart1 = Cart::where('customer_id',$request->customer_id)
                       ->where('product_id',$request->product_id)->first();
                       $cart_count = $cart1->quantity;

                       $c = $product_stock - $cart_count;

                       $update = Product::where('id',$request->product_id)
                        ->update(['stock'=>$c]);


                            return response()->json(['status'=>1,'response'=>"Cart Updated  Successfully"]);
                      

                

                } catch (\Exception $e) {
                    throw $e;
                    return response()->json(['status'=>0,'response'=>"Not Inserted Data"]);
                }
            
        }

    
}
