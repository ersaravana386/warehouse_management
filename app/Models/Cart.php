<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;
    
    protected $table="carts";

            public function product()
                {
                    return $this->belongsTo('App\Models\Product','product_id','id');
                }
            public function customer()
            {
                return $this->belongsTo('App\Models\Customer','customer_id','id');
            }
        
}
