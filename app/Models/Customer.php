<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;
    protected $table= 'customers';

    public function cart()
    {
        return $this->HasMany('App\Models\Cart','customer_id','id');
    }

   
}
